package arapp.darioalmagro.facci.practica4c;

import android.app.Dialog;
import android.net.Uri;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, FrgUno.OnFragmentInteractionListener, FrgDos.OnFragmentInteractionListener  {

    Button botonFrgUno, botonFrgDos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        botonFrgUno = (Button) findViewById(R.id.btnFrgUno);
        botonFrgDos = (Button) findViewById(R.id.btnFrgDos);

        botonFrgUno.setOnClickListener(this);
        botonFrgDos.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
     MenuInflater inflater = getMenuInflater();
     ((MenuInflater) inflater).inflate(R.menu.main, menu);
     return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.opcionLogin:
                Dialog dialogLogin = new Dialog(MainActivity.this);
                dialogLogin.setContentView(R.layout.dlg_log);
                Button botonAutenticar = (Button) dialogLogin.findViewById(R.id.btnAutenticar);
                final EditText cajaUsuario = (EditText) dialogLogin.findViewById(R.id.txtUser);
                final EditText cajaClave = (EditText) dialogLogin.findViewById(R.id.txtPassword);

                botonAutenticar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(MainActivity.this, cajaUsuario.getText().toString() + " " + cajaClave.getText().toString(), Toast.LENGTH_LONG).show();
                    }
                });
                dialogLogin.show();
                break;
            case R.id.opcionRegistrar:
                break;
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnFrgUno:
                FrgUno fragmentoUno = new FrgUno();
                FragmentTransaction transactionUno = getSupportFragmentManager().beginTransaction();
                transactionUno.replace(R.id.contenedor,fragmentoUno);
                transactionUno.commit();
                break;
            case R.id.btnFrgDos:
                FrgDos fragmentoDos = new FrgDos();
                FragmentTransaction transactionDos = getSupportFragmentManager().beginTransaction();
                transactionDos.replace(R.id.contenedor,fragmentoDos);
                transactionDos.commit();
                break;
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
